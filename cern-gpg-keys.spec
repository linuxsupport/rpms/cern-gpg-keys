Name:		cern-gpg-keys
Version:	1.0
Release:	1%{?dist}
Summary:	CERN RPM keys
Group:		System Environment/Base
License:	GPLv2
URL:		http://linux.cern.ch/
BuildArch:  noarch
Source0:    %{name}-%{version}.tgz

%description
This package provides the RPM signature keys for CERN packages.

%prep
%setup -q -n %{name}-%{version}

%build

%install
install -d -m 0755 %{buildroot}%{_sysconfdir}/pki/rpm-gpg
install -p -m 0644 RPM-GPG-KEY-kojiv2 %{buildroot}%{_sysconfdir}/pki/rpm-gpg/

%files
%defattr(-,root,root,-)
%{_sysconfdir}/pki/rpm-gpg

%changelog
* Tue Aug 30 2022 Alex Iribarren <Alex.Iribarren@cern.ch> 1.0-1
- Initial release
